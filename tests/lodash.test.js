const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})


// ADD NEW TEST //

test('Should create an array with all falsey values removed.', () => {
    const input = [0, 1, false, 2, '', 3];
    let ans = _.compact(input);
    expect(ans).to.eql([1, 2, 3]);
})

test('Should create an array of unique values that are included in all given arrays using SameValueZero for equality comparisons.', () => {
    const input1 = [2, 1];
    const input2 = [1, 2, 3]
    let ans = _.intersection(input1, input2);
    expect(ans).to.eql([2, 1]);
})

test('Should convert all elements in array into a string separated by separator.', () => {
    const input = ['a', 'b', 'c'];
    const sep =  '~';
    let ans = _.join(input, sep);
    expect(ans).to.eql('a~b~c');
})

test('Should get the last element of array.', () => {
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    let ans = _.last(input);
    expect(ans).to.eql(9);
})

test('Should gets the element at index n of array.', () => {
    const input = ['a', 'b', 'c', 'd'];
    const choose = 2;
    let ans = _.nth(input, choose);
    expect(ans).to.eql('c');
})

test('Should create an array of unique values that is the symmetric difference of the given arrays.', () => {
    const input1 = [2, 1];
    const input2 = [2, 3];
    let ans = _.xor(input1, input2);
    expect(ans).to.eql([1, 3]);
})

test('Should get the size of collection by returning its length for array-like values or the number of own enumerable string keyed properties for objects.', () => {
    const input = [1, 2, 3, 3, 3];
    let ans = _.size(input);
    expect(ans).to.eql(5);
})

test('Should compute number rounded to precision.', () => {
    const input = 4.006;
    const preci = 2;
    let ans = _.round(input, preci);
    expect(ans).to.eql(4.01);
})

test('Should clamp number within the inclusive lower and upper bounds.', () => {
    const input = 10;
    const lb = -1;
    const ub = 1;
    let ans = _.clamp(input, lb, ub);
    expect(ans).to.eql(1);
})

test('Should repeat the given string n times.', () => {
    const input = 'abc';
    const times = 5
    let ans = _.repeat(input, times);
    expect(ans).to.eql('abcabcabcabcabc');
})